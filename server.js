// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

var Firebase = require("firebase");

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// more routes for our API will happen here

// on routes that end in /register
// ----------------------------------------------------
router.route('/register')

    // create an account (accessed at POST http://localhost:8080/api/register)
    .post(function(req, res) {

        console.log('Email: ' + req.body.email);
        console.log('Password: ' + req.body.password);

        var email = req.body.email;
        var password = req.body.password;

        var firebaseRef = new Firebase("https://game-service.firebaseio.com");
        firebaseRef.createUser({
          email: email,
          password: password
        }, function(error, userData) {
          if (error) {
            switch (error.code) {
              case "EMAIL_TAKEN":
                res.json({ error: 1, code: "EMAIL_TAKEN",message: 'The new user account cannot be created because the email is already in use.' });
                break;
              case "INVALID_EMAIL":
                res.json({ error: 1, code: "INVALID_EMAIL",message: 'The specified email is not a valid email.' });
                break;
              default:
                res.json({ error: 1, code: "Unknown",message: 'Unknown' });
            }
          } else {
            res.json({ error: 0, userId: userData.uid });
          }
        });
    });

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
