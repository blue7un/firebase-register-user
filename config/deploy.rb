require 'mina/bundler'
require 'mina/rails'
require 'mina/git'

# Basic settings:
# domain     - The hostname to SSH to
# deploy_to  - Path to deploy into
# repository - Git repo to clone from (needed by mina/git)
# user       - Username in the  server to SSH to (optional)

set :domain, '139.162.231.157'
set :deploy_to, '/home/deploy/firebase_register'
set :repository, 'git@bitbucket.org:blue7un/firebase-register-user.git'
set :user, 'deploy'
# set :port, '30000'

desc "Deploys the current version to the server."
 task :deploy do
   deploy do
     invoke :'git:clone'
     to :launch do
       queue 'npm install --production'
       invoke :'start'
     end

  end
end

task :start do
 queue %[cd #{deploy_to}/current && passenger-config restart-app $(pwd)]
end